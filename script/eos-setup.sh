#!/bin/bash

# April 2016 Borja Aparicio
# 2018 Aurelijus Rinkevicius
# Receives:
# Environment variables
#   EOS_ACCOUNT_USERNAME
#   EOS_ACCOUNT_PASSWORD
#   EOS_PATH
#   EOS_MGM_URL => default: root://eosuser.cern.ch
#

# Authenticate user via Kerberos
kinit="/usr/bin/kinit"
if [ ! -x $kinit ]
then
    echo "ERROR: $kinit not found"
    exit 1
fi

kdestroy="/usr/bin/kdestroy"
if [ ! -x $kdestroy ]
then
    echo "ERROR: $kdestroy not found"
    exit 1
fi

# XROOTD client to copy files to/from EOS
xrdcp="/usr/bin/xrdcp"
if [ ! -x $xrdcp ]
then
    echo "ERROR: $xrdcp not found"
    exit 1
fi

# Validate input
: "${EOS_ACCOUNT_USERNAME:?EOS_ACCOUNT_USERNAME not provided}"
: "${EOS_ACCOUNT_PASSWORD:?EOS_ACCOUNT_PASSWORD not provided}"
: "${EOS_PATH:?EOS_PATH not provided}"

# EOS MGM URL, if not provided by the user
if [ "X$EOS_MGM_URL" == "X" ]
then
    EOS_MGM_URL="root://eosuser.cern.ch"
fi

# Get credentials
echo "$EOS_ACCOUNT_PASSWORD" | $kinit "$EOS_ACCOUNT_USERNAME@CERN.CH" 2>&1 >/dev/null
if [ $? -ne 0 ]
then
    echo "Failed to get Krb5 credentials for '$EOS_ACCOUNT_USERNAME'"
    exit 1
fi
