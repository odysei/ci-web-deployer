FROM gitlab-registry.cern.ch/linuxsupport/cc7-base:latest
MAINTAINER borja.aparicio.cotarelo@cern.ch

# Install Kerberos client, rsync and ssh
RUN /usr/bin/yum install -y krb5-workstation xrootd-client

# Install samba client tools to connect via smbclient to DFS filesystem
RUN /usr/bin/yum install -y samba-client

# Script that will copy the web contents the the DFS Web site
COPY deploy-dfs.sh /sbin/deploy-dfs
RUN chmod 755 /sbin/deploy-dfs

# Script that will rsync the output generated to an EOS folder (could be an EOS Web site)
COPY deploy-eos.sh /sbin/deploy-eos
RUN chmod 755 /sbin/deploy-eos

# Script that will copy a file from an EOS folder
COPY get-eos-file.sh /sbin/get-eos-file
COPY script/eos-setup.sh /sbin/script/eos-setup.sh
RUN chmod 755 /sbin/get-eos-file
