#!/bin/bash

parent_path=$(cd "$(dirname "${BASH_SOURCE[0]}")"; pwd -P)
. ${parent_path}/script/eos-setup.sh

# Rely in xrootd to do the copy of files from EOS. The destination path is 
# created as necessary.
$xrdcp --force --recursive --silent --path "$EOS_MGM_URL/$1" "$2/"
if [ $? -ne 0 ]
then
    echo "ERROR: Failed to copy file '$1' to '$2/' via xrdcp"
    exit 1
fi

# Destroy credentials
$kdestroy
if [ $? -ne 0 ]
then
    echo "Krb5 credentials for '$DFS_ACCOUNT_USERNAME' have not been cleared up"
fi

echo "Transferred from EOS '$1'"
exit 0
